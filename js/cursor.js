let myCanvas;
let angle = 0;
let num = 13;
let size = 50;
let x=0;
let y=0;
function setup() {
    myCanvas = createCanvas(windowWidth, windowHeight);
    myCanvas.parent("p5Wrapper");
    colorMode(RGB);
    angleMode(DEGREES);
}

function draw() {
    clear();
/*     fill(56, 74, 211, 50);
    noStroke();
    ellipse(mouseX, mouseY, size);

    push();
    translate(mouseX, mouseY);
    fill(248, 215, 28, 150);
    rotate(angle);
    angle+=0.5;
    for(let i= 0; i<num; i++){
        let cir = 360/num;
        rotate(cir);
        ellipse(0, size, 20, size);
    }
    pop(); */

    noStroke();
    
    ellipse(mouseX, mouseY, 10);
    x += (mouseX - x) * 0.04;
    y += (mouseY - y) * 0.04;
    translate(x,y);
    fill(248, 215, 28, 190);
    push();
    rotate(angle);
    angle+=0.8;
    textSize(30);
    text("✿", 0, 0);
    pop();
}

/*function mouseClicked(){
    console.log("clicked!");
    
    flowerBackground.fill(248, 215, 28, 150);
    flowerBackground.textSize(30);
    flowerBackground.text("✿", mouseX, mouseY);
}*/

function windowResized(){
    resizeCanvas(windowWidth, windowHeight);
}